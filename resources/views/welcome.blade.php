@extends('layouts.master')

@section('content')

    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <router-link to="/">
                        <img class="img-responsive" src="{{ url('/img/logo.png') }}"/>
                    </router-link>
                </div>
            </div>
        </div>
    </header>

    <div class="main">
        <div class="container">
            {{--<sales-offer-form></sales-offer-form>--}}
            <router-view></router-view>
        </div>
    </div>

    <footer>

    </footer>

@endsection
