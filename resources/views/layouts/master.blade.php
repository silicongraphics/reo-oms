<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="api-base-url" content="{{ url('') }}" />

    <title>{{ config('app.name', 'REO OMS') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script>
        window.baseUrl = '{{ url('/') }}'
    </script>

    <style>
        #app {
            display: flex;
            flex-direction: column;
            height: 100%;
        }
        #app .main {
            margin: auto;
        }
        body,html {
            height: 100%;
        }
    </style>

</head>
<body>
    <div id="app">

        @yield('content')

    </div>
</body>
</html>
