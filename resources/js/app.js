
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

import {
    HasError,
    AlertError,
    AlertErrors,
    AlertSuccess
} from 'vform'

Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);
Vue.component(AlertErrors.name, AlertErrors);
Vue.component(AlertSuccess.name, AlertSuccess);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

import HomeComponent from './components/HomeComponent';
import OffPlanOfferComponent from './components/OffPlanOfferComponent';
import PlotOfferComponent from './components/PlotOfferComponent';
import NotFoundComponent from './components/NotFoundComponent';

// Vue.component('sales-offer-form', require('./components/SalesOfferComponent').default);
// Vue.component('home-component', require('./components/HomeComponent').default);

const routes = [
    { name: '', path: '/', component: HomeComponent },
    { name: 'salesOffer1', path: '/off-plan-offer', component: OffPlanOfferComponent },
    { name: 'salesOffer2', path: '/plot-offer', component: PlotOfferComponent },
    { path: '/404', component: NotFoundComponent },
    { path: '*', redirect: '/404' },
];

const router = new VueRouter({
    base: process.env.NODE_ENV === 'test'? '/al-ruwad/public/': '',
    mode: 'history',
    routes
});

const app = new Vue({
    el: '#app',
    components: { HomeComponent },
    router
});

