<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaleOfferStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customerName'                                          => 'required',
            'welcomeNote'                                           => 'required',
            'brokerName'                                            => 'required',
            'brokerDesignation'                                     => 'required',
            'brokerRegistrationNumber'                              => 'required',
            'projects'                                              => 'required|array',
            'projects.*.buildArea'                                  => 'required',
            'projects.*.plotNo'                                     => 'required',
            'projects.*.plotSize'                                   => 'required',
            'projects.*.plotUse'                                    => 'required',
            'projects.*.pricePerFit'                                => 'required',
            'projects.*.projectLocation'                            => 'required',
            'projects.*.totalPrice'                                 => 'required|numeric',

            'projects.*.paymentPlan'                                => 'required',
            'projects.*.paymentPlan.noYears'                        => 'required',
            'projects.*.paymentPlan.milestones'                     => 'required|array',
            'projects.*.paymentPlan.milestones.*.milestone'         => 'required',
            'projects.*.paymentPlan.milestones.*.percentage'        => 'required|numeric',
            // 'projects.*.notes'          => 'required|array',
            // 'projects.*.paymentPlan.milestones.*.date'             => 'required',

        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'projects.*.buildArea.required'                                 => 'Building Area is required',
            'projects.*.noYears.required'                                   => 'No of years is required',
            'projects.*.milestones.required'                                => 'At least one milestone is required',
            'projects.*.plotNo.required'                                    => 'Plot no. is required',
            'projects.*.notes.required'                                     => 'At least one Note is required',

            'projects.*.plotSize.required'                                  => 'Plot Size is required',
            'projects.*.plotUse.required'                                   => 'Plot Use is required',
            'projects.*.pricePerFit.required'                               => 'Price per fit is required',
            'projects.*.projectLocation.required'                           => 'Location is required',
            'projects.*.totalPrice.required'                                => 'Total Price is required',
            'projects.*.totalPrice.numeric'                                 => 'Total Price must be numeric value',

            'projects.*.paymentPlan.milestones.*.milestone.required'         => 'Milestone is required',
            'projects.*.paymentPlan.milestones.*.percentage.required'        => 'Percentage is required',
            // 'projects.*.paymentPlan.milestones.*.date.required'              => 'Date is required',
        ];
    }
}
