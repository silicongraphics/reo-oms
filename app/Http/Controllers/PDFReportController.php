<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;


class PDFReportController extends Controller
{
    public function index(Request $request)
    {
        $offer = session('offer');
        if(!$offer){
            return redirect('/');
        }

        $pdf = PDF::loadView('reports.pdf', compact('offer'));
        return $pdf->download(time().'_sales_offer.pdf');
//         return $pdf->stream('invoice.pdf');


        /*$pdf = \PDF::loadView('reports.pdf', compact('offer'));
        return $pdf->save('document.pdf');

        return view('reports.pdf', compact('offer'));*/
    }

    public function salesReportTwo(Request $request)
    {
        $offer = session('offer2');
        if(!$offer){
            return redirect('/');
        }

        $pdf = PDF::loadView('reports.pdf2', compact('offer'));
        return $pdf->download(time().'_sales_offer.pdf');
//         return $pdf->stream('invoice.pdf');


        /*$pdf = \PDF::loadView('reports.pdf', compact('offer'));
        return $pdf->save('document.pdf');

        return view('reports.pdf', compact('offer'));*/
    }

}
