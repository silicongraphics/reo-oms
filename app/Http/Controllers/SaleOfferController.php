<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaleOffer2Store;
use App\Http\Requests\SaleOfferStore;
use Illuminate\Http\Request;


class SaleOfferController extends Controller
{
    public function store(SaleOfferStore $request)
    {
        session()->put('offer', $request->all());

        return session('offer');
    }

    public function store2(SaleOffer2Store $request)
    {
        session()->put('offer2', $request->all());

        return session('offer2');
    }

}
