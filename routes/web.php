<?php
Route::get('/clear', function () {
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('config:cache');
    return 'done';
});

Route::get('/migrate', function () {
    \Illuminate\Support\Facades\Artisan::call('migrate', ['--seed' => true]);
    return 'done';
});

Route::get('/migrate-refresh', function () {
    \Illuminate\Support\Facades\Artisan::call('migrate:refresh', ['--seed' => true]);
    return 'done';
});

Route::get('/notes/', 'NotesController@index');
Route::get('/notes/{form}', 'NotesController@index');

Route::post('/sale-offers', 'SaleOfferController@store');
Route::post('/sale-offers-2', 'SaleOfferController@store2');

Route::get('sale-offers/docs', 'WordReportController@index');
Route::get('sale-offers/excel', 'ExcelReportController@index');
Route::get('sale-offers/pdf', 'PDFReportController@index');

Route::get('sale-offers-2/docs/{second}', 'WordReportController@index');
Route::get('sale-offers-2/excel', 'ExcelReport2Controller@index');
Route::get('sale-offers-2/pdf', 'PDFReportController@salesReportTwo');

Route::get('/{any}', function () { return view('welcome'); })->where('any', '.*');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
